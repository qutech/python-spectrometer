# python-spectrometer
This package implements data acquisition, processing, and visualization for estimating power spectral densities using [Welch's method](https://en.wikipedia.org/wiki/Welch%27s_method). It provides the `Spectrometer` class that serves as a central interface which acquires and manges the data. Several processing steps can be applied to the raw timeseries data, for instance to convert from a voltage signal to an acceleration given a known calibration from a signal conditioning unit.

To demonstrate the basic features, here is some example code using the Keysight DMM `qcodes` driver for data acquisition:
```python
from python_spectrometer import Spectrometer, daq
from qcodes.instrument_drivers.Keysight.Keysight_34465A_submodules import Keysight_34465A
dmm = Keysight_34465A('dmm', 'some_tcpip_address')

# Pre-defined functions that set up and execute a measurement using a DMM
spect = Spectrometer(daq.qcodes.Keysight344xxA(dmm),
                     procfn=lambda V: V*1000,
                     processed_unit='mV')
settings = {'f_min': 0.1, 'f_max': 1000, 'phase_of_the': 'moon'}  # any other settings or metadata
spect.take('a comment', n_avg=5, **settings)
spect.hide(0)
spect.show('a comment')  # same as spect.show(0)
# Save and recall functionality
spect.serialize_to_disk('./foo')
spect_loaded = Spectrometer.recall_from_disk('./foo')  # read-only because no DAQ given
spect_loaded.show_keys()
# (0, 'a comment')
```

You can also play around with simulated noise (requires `qopt`):
```python
from python_spectrometer import Spectrometer, daq
spect = Spectrometer(daq.simulator.QoptColoredNoise(lambda f, A, **_: A/f))
spect.take('foobar', n_avg=10, n_seg=5, A=42)
```

## Installing
If you just want to use it you can install the latest "released" version via
```sh
python -m pip install python-spectrometer[complete]
```
However, this package profits from everybody's work and the releases are infrequent. Please make a development install
and contribute your changes. You can do this via
```sh
python -m pip install -e git+https://git.rwth-aachen.de/qutech/python-spectrometer.git#egg=python-spectrometer[complete]
```
This will download the source code (i.e. clone the git repository) into a subdirectory of the `./src` argument and link the files into your environment instead of copying them. If you are on Windows you can use [SourceTree](https://www.sourcetreeapp.com/) which is a nice GUI for git.
You can specify the source code directory with the `--src` argument (which needs to be BEFORE `-e`):
```sh
python -m pip install --src some_directory/my_python_source -e git+https://git.rwth-aachen.de/qutech/python-spectrometer.git#egg=python-spectrometer[complete]
```
If you have already downloaded/cloned the package yourself you can use `python -m pip install -e .[complete]`.

Please file an issue if any of these instructions does not work.

## Documentation
Some of the development of this package took place during a course taught at the II. Institute of Physics at RWTH Aachen University in the winter semester 2022/23. Targeting applied research topics too specific for lectures but too general for lab courses, several modules intended for self-learning were developed, one of which focuses on "characterizing and avoiding noise and interference in instrumentation". The material can be found here: 
- [Part 1](https://iffmd.fz-juelich.de/s/6sxq2OgNO),
- [Part 2](https://iffmd.fz-juelich.de/s/7LQ7xCCNJ).

The `python_spectrometer` package has an auto-generated documentation that can be found at [the Gitlab Pages](https://qutech.pages.rwth-aachen.de/python-spectrometer/index.html).

To build the documentation locally, navigate to `doc/` and run
```sh
make html
```
or
```bat
sphinx-build -b html source build
```
Make sure the dependencies are installed via
```sh
python -m pip install -e .[doc]
```
in the top-level directory. 

To check if everything works for a clean install (requires hatch to be installed), run
```sh
python -m hatch run doc:build
```

## Tests
There are some basic tests in `tests/` as well as a couple of [`doctests`](https://docs.python.org/3/library/doctest.html).

You can run the tests either via
```sh
python -m pytest --doctest-modules
```
or to check if everything works for a clean install (requires hatch to be installed)
```sh
python -m hatch run tests:run
```

## Releases
Releases on Gitlab, PyPI, and Zenodo are automatically created and pushed whenever a commit is tagged matching [CalVer](https://calver.org/) in the form `vYYYY.MM.MICRO` or `vYYYY.0M.MICRO`.
