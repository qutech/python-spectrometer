"""Mock imports of optional DAQ dependencies that cannot be installed
from PyPI."""

import sys
from types import ModuleType
from unittest.mock import Mock

sys.modules['atsaverage'] = ModuleType('atsaverage')

sys.modules['atsaverage.core'] = ModuleType('atsaverage.core')
sys.modules['atsaverage.core'].AlazarCard = Mock()

sys.modules['atsaverage.masks'] = ModuleType('atsaverage.masks')
sys.modules['atsaverage.masks'].PeriodicMask = Mock()

sys.modules['atsaverage.operations'] = ModuleType('atsaverage.operations')
sys.modules['atsaverage.operations'].Downsample = Mock()

sys.modules['atsaverage.alazar'] = ModuleType('atsaverage.alazar')
sys.modules['atsaverage.alazar'].InputRangeID = Mock()

sys.modules['atsaverage.config2'] = ModuleType('atsaverage.config2')
sys.modules['atsaverage.config2'].BoardConfiguration = Mock()
sys.modules['atsaverage.config2'].EngineTriggerConfiguration = Mock()
sys.modules['atsaverage.config2'].CaptureClockConfiguration = Mock()
sys.modules['atsaverage.config2'].CaptureClockType = Mock()
sys.modules['atsaverage.config2'].SampleRateID = Mock()
sys.modules['atsaverage.config2'].InputConfiguration = Mock()
sys.modules['atsaverage.config2'].Channel = Mock()
sys.modules['atsaverage.config2'].create_scanline_definition = Mock()

sys.modules['atssimple'] = ModuleType('atssimple')
sys.modules['atssimple'].ATSSimpleCard = Mock()
sys.modules['atssimple'].acquire_downsample_windows = Mock()
sys.modules['atssimple'].atsapi = Mock()

sys.modules['TimeTagger'] = ModuleType('TimeTagger')
