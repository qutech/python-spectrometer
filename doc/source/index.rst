.. python-spectrometer documentation master file, created by
   sphinx-quickstart on Wed Jun  8 12:00:57 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to python_spectrometer's documentation!
===============================================

.. include:: readme_link.md
   :parser: myst_parser.sphinx_

python_spectrometer API Documentation
-------------------------------------

.. autosummary::
   :toctree: _autogen
   :recursive:

   python_spectrometer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
