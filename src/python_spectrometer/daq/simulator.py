"""Provides a simulation backend for data acquisition.

Examples
--------
>>> import python_spectrometer as pyspeck
>>> import tempfile
>>> speck = pyspeck.Spectrometer(pyspeck.daq.QoptColoredNoise(),
...                              savepath=tempfile.mkdtemp())
>>> speck.take('a test', fs=10e3)  #doctest: +ELLIPSIS
...

Add an artificial time delay to mimick finite data acquisition time:
>>> speck.take('delayed', n_avg=3, delay=True)  #doctest: +ELLIPSIS
...

"""
from __future__ import annotations

import dataclasses
import sys
import time
from typing import Callable, Iterator

import numpy as np
from qutil.functools import partial

from .base import DAQ

if sys.version_info >= (3, 13):
    from warnings import deprecated
else:
    from typing_extensions import deprecated
try:
    from numpy.typing import NDArray
except ImportError:
    from numpy import ndarray as NDArray

try:
    import qopt
except ImportError as e:
    raise ImportError('This simulated DAQ requires qopt. You can install it by running '
                      "'pip install qopt.'") from e


@dataclasses.dataclass
class QoptColoredNoise(DAQ):
    """Simulates noise using :mod:`qopt:qopt`.

    See :class:`~python_spectrometer.core.Spectrometer` for
    more details on usage and
    :class:`~python_spectrometer.daq.settings.DAQSettings`
    for more information on setup parameters.

    See Also
    --------
    :func:`qopt:qopt.noise.fast_colored_noise`
        For information on the simulation.

    """
    spectral_density: Callable[[NDArray, ...], NDArray] = dataclasses.field(
        default_factory=lambda: QoptColoredNoise.white_noise
    )
    """A callable with signature::

        f(ndarray, **settings) -> ndarray

    that returns the power spectral density for given frequencies.
    Defaults to white noise with scale parameter ``S_0``.
    """

    @staticmethod
    def white_noise(f, S_0: float = 1.0, **_) -> NDArray:
        """White noise power spectral density with amplitude S_0."""
        return np.full_like(f, S_0)

    def acquire(self, *, n_avg: int, fs: float, n_pts: int, delay: bool | float = False,
                **settings) -> Iterator[NDArray]:
        """Executes a measurement and yields the resulting timetrace.

        Optionally set a delay to simulate a finite data acquisition
        time. If True, delays by the amount of time it would take to
        actually acquire data with the given settings; if float delay
        by the given amount.
        """
        if delay is True:
            delay = 1 / settings['df']

        for _ in range(n_avg):
            tic = time.perf_counter()
            data = qopt.noise.fast_colored_noise(
                partial(
                    settings.get('spectral_density', self.spectral_density),
                    **settings
                ),
                dt=1/fs, n_samples=n_pts, output_shape=()
            )
            if delay:
                time.sleep(delay - (time.perf_counter() - tic))
            yield data

        # This is the place to return metadata (possibly obtained from the instrument)
        return {'qopt version': qopt.__version__}


@deprecated("Use QoptColoredNoise instead")
class qopt_colored_noise(QoptColoredNoise):
    ...
