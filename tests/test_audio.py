import os
from tempfile import mkdtemp
from unittest import mock

import pytest

from python_spectrometer import Spectrometer
from python_spectrometer.daq import QoptColoredNoise


@pytest.fixture
def import_or_mock_pyaudio():
    if os.environ.get('GITLAB_CI', 'false').lower() == 'true':
        with mock.patch('python_spectrometer._audio_manager.pyaudio') as mock_pyaudio:
            yield mock_pyaudio
    else:
        import pyaudio
        yield pyaudio


def test_audio(import_or_mock_pyaudio):
    spect = Spectrometer(QoptColoredNoise(), savepath=mkdtemp(),
                         play_sound=True)
    spect.take('with sound', f_max=20000, A=2e-4)

    assert 'audio_stream' in spect.__dict__
    assert spect.audio_stream.playback_thread is not None

    spect.audio_stream.stop()

    assert not spect.audio_stream.playback_thread.is_alive()

    if isinstance(import_or_mock_pyaudio, mock.Mock):
        import_or_mock_pyaudio.PyAudio.return_value.open.assert_called_once_with(
            format=import_or_mock_pyaudio.paFloat32,
            channels=1,
            rate=44100,
            output=True
        )

    spect.play_sound = False

    assert 'audio_stream' not in spect.__dict__

    spect.take('without sound', f_max=20000, A=2e-4)

    assert 'audio_stream' not in spect.__dict__

    if isinstance(import_or_mock_pyaudio, mock.Mock):
        import_or_mock_pyaudio.PyAudio.return_value.open.assert_called_once()
