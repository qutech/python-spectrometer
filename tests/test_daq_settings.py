from unittest.mock import MagicMock

import pytest
from qutil import itertools
from zhinst import toolkit

from python_spectrometer.daq import DAQSettings
from python_spectrometer.daq.settings import ResolutionError
from python_spectrometer.daq.zurich_instruments import (ZurichInstrumentsMFLIDAQ,
                                                        ZurichInstrumentsMFLIScope)


class MockSession(MagicMock):
    def connect_device(self, *args, **kwargs):
        return MockDevice()


class MockDevice(MagicMock):
    def clockbase(self):
        return 60e6

    @property
    def device_type(self):
        return 'MFLI'


@pytest.fixture(params=[ZurichInstrumentsMFLIDAQ, ZurichInstrumentsMFLIScope])
def mock_zi_daq(monkeypatch, request):

    monkeypatch.setattr(toolkit, 'Session', MockSession(spec=toolkit.Session))
    session = toolkit.Session('localhost')

    return request.param(session, session.connect_device('dev1234'))


def test_daq_settings_warnings():

    with pytest.warns(UserWarning, match='Need to change df from 0.999 to 0.998003992015968.'):
        s = DAQSettings(nperseg=1000, df=0.999)
        s.fs = 1000

    with pytest.warns(UserWarning, match='Need to change fs from 1001 to 1000.'):
        s = DAQSettings(fs=1001, df=1)
        s.nperseg = 1000


def test_daq_settings_exceptions():

    with pytest.raises(ResolutionError):
        # f_min < df
        s = DAQSettings(df=23, f_min=11)
        s.to_consistent_dict()

    with pytest.raises(ResolutionError):
        # f_max > fs/2
        s = DAQSettings(fs=1234.5, f_max=903.012)
        s.to_consistent_dict()


@pytest.mark.filterwarnings("ignore:Need to change")
def test_daq_settings():

    s = DAQSettings(nperseg=321, noverlap=100)
    t = DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(nperseg=321, noverlap=100, fs=450.21)
    t = DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(noverlap=100, fs=450.21)
    t = DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(n_pts=1234, nperseg=321, f_min=3)
    t = DAQSettings(s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(f_min=10, fs=14648.4375)
    t = DAQSettings(s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(fs=14648.4375, n_pts=16384)
    t = DAQSettings(s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(n_pts=2**14, fs=26785.714285714286, freq=500)
    t = DAQSettings(s.to_consistent_dict())
    t.to_consistent_dict()

    s = DAQSettings(n_pts=10**5, fs=10e3)
    assert s.to_consistent_dict()['n_seg'] < 10

    s = DAQSettings(n_pts=10**5, fs=10e3, nperseg=1024)
    assert s.to_consistent_dict()['n_seg'] < 1000


@pytest.mark.filterwarnings("ignore:Need to change")
def test_mfli_daq_settings(mock_zi_daq):

    s = mock_zi_daq.DAQSettings(f_max=1900)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(fs=1900)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(fs=1900, f_max=1000)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(f_min=0.1)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(df=0.1)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(df=0.1, f_min=0.1)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(df=23, f_min=11)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(fs=40e3)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()

    s = mock_zi_daq.DAQSettings(fs=40e3, nperseg=10e3)
    t = mock_zi_daq.DAQSettings(**s.to_consistent_dict())
    t.to_consistent_dict()


def test_reproducibility():
    """Test if there are no exceptions or infinite recursions for a
    consistent set of parameters (only test up to 4 keyword parameters
    since the product scales exponentially...)
    """
    s = DAQSettings().to_consistent_dict()
    for kv in itertools.product(s.items(), repeat=4):
        t = DAQSettings(**dict(kv))
        d = t.to_consistent_dict()
        # Make sure this is reproducible
        _ = DAQSettings(d).to_consistent_dict()
        for k in set(s).difference(set(t)):
            # use setters for remaining parameters
            setattr(t, k, s[k])
