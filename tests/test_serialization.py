import os
import pathlib
import random
import string
from pathlib import Path
from tempfile import mkdtemp
from typing import Any, Generator

import pytest

from python_spectrometer import Spectrometer, daq


def remove_file_if_exists(file):
    try:
        os.remove(file)
    except (OSError, FileNotFoundError):
        pass


def remove_dir_if_exists(d):
    try:
        os.rmdir(d)
    except (OSError, FileNotFoundError):
        pass


@pytest.fixture(params=[True, False])
def spectrometer(monkeypatch, request) -> Generator[Spectrometer, Any, None]:
    # patch input to answer overwrite queries with "yes"
    monkeypatch.setattr('builtins.input', lambda: 'y')

    speck = Spectrometer(daq.QoptColoredNoise(),
                         savepath=pathlib.Path(cwd := mkdtemp(), 'test_data'),
                         plot_cumulative=True,
                         relative_paths=request.param)
    speck.savepath.mkdir(parents=True, exist_ok=True)

    try:
        os.chdir(speck.savepath)

        speck.take('foo')
        speck.take('baz', fs=1e3, nperseg=400)

        yield speck
    finally:
        for file in speck.files:
            remove_file_if_exists(file)
        remove_dir_if_exists(speck.savepath)

        os.chdir(cwd)


@pytest.fixture
def serialized(spectrometer: Spectrometer) -> Generator[Path, Any, None]:
    stem = ''.join(random.choices(string.ascii_letters, k=10))

    try:
        spectrometer.serialize_to_disk(stem)

        yield spectrometer.savepath / stem
    finally:
        exts = ['_files.txt']
        if (spectrometer.savepath / stem).is_file():
            remove_file_if_exists(spectrometer.savepath / stem)
        else:
            exts.extend(['.bak', '.dat', '.dir'])
        for ext in exts:
            remove_file_if_exists(spectrometer.savepath / f'{stem}{ext}')


def test_saving(spectrometer: Spectrometer):
    assert spectrometer.savepath.exists()
    for file in spectrometer.files:
        assert os.path.exists(file)


def test_serialization(spectrometer: Spectrometer):
    spectrometer.serialize_to_disk('blub')

    exts = ['_files.txt']
    if (spectrometer.savepath / 'blub').is_file():
        assert os.path.exists(spectrometer.savepath / 'blub')
    else:
        exts.extend(['.bak', '.dat', '.dir'])
    for ext in exts:
        assert os.path.exists(spectrometer.savepath / f'blub{ext}')


def test_deserialization(serialized: pathlib.Path):
    speck = Spectrometer.recall_from_disk(serialized)
    for data, comment in zip(speck, ['foo', 'baz']):
        assert data['comment'] == comment
    assert speck['baz']['settings']['fs'] == 1e3
    assert speck['baz']['settings']['nperseg'] == 400
    assert speck.plot_cumulative is True
